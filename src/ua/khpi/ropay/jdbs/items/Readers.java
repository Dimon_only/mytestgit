package ua.khpi.ropay.jdbs.items;

public class Readers {
	
	
	
	private int keyReader;
	private String nameReader;
	private String surname;
	private String middleName;
	private String phoneNumber;
	private String adress;
	private String yearOfBirth;
	
	public Readers() {
		
	}
	
	public Readers(int keyReader, String nameReader, String surname, String middleName, String phoneNumber, String adress, String yearOfBirht) {
		
		this.keyReader = keyReader;
		this.nameReader = nameReader;
		this.surname = surname;
		this.middleName = middleName;
		this.phoneNumber = phoneNumber;
		this.adress = adress;
		this.yearOfBirth = yearOfBirht;
		
	}
	
	public int getKeyReader() {
		return keyReader;
	}
	public void setKeyReader(int keyReader) {
		this.keyReader = keyReader;
	}
	public String getNameReader() {
		return nameReader;
	}
	public void setNameReader(String nameReader) {
		this.nameReader = nameReader;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public String getMiddleName() {
		return middleName;
	}
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getAdress() {
		return adress;
	}
	public void setAdress(String adress) {
		this.adress = adress;
	}
	public String getYearOfBirth() {
		return yearOfBirth;
	}
	public void setYearOfBirth(String yearOfBirth) {
		this.yearOfBirth = yearOfBirth;
	}
	
	@Override
	public String toString() {
		return "Readers [keyReader=" + keyReader + ", nameReader=" + nameReader + ", surname=" + surname
				+ ", middleName=" + middleName + ", phoneNumber=" + phoneNumber + ", adress=" + adress
				+ ", yearOfBirth=" + yearOfBirth + "]";
	}

}
