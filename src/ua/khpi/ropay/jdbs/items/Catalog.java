package ua.khpi.ropay.jdbs.items;

public class Catalog {

	private int key;
	private int keyBook;
	private int keyReader;
	private String dateTaken;
	private String dateReturn;
	private int debt;
	private int fine;

	public Catalog() {

	}

	public Catalog(int key, int keyBook, int keyReader, String dateTaken, String dateReturn, int debt, int fine) {

		this.key = key;
		this.keyBook = keyBook;
		this.keyReader = keyReader;
		this.dateTaken = dateTaken;
		this.dateReturn = dateReturn;
		this.debt = debt;
		this.fine = fine;

	}

	public int getKey() {
		return key;
	}

	public void setKey(int key) {
		this.key = key;
	}

	public int getKeyBook() {
		return keyBook;
	}

	public void setKeyBook(int keyBook) {
		this.keyBook = keyBook;
	}

	public int getKeyReader() {
		return keyReader;
	}

	public void setKeyReader(int keyReader) {
		this.keyReader = keyReader;
	}

	public String getDateTaken() {
		return dateTaken;
	}

	public void setDateTaken(String dateTaken) {
		this.dateTaken = dateTaken;
	}

	public String getDateReturn() {
		return dateReturn;
	}

	public void setDateReturn(String dateReturn) {
		this.dateReturn = dateReturn;
	}

	public int getDebt() {
		return debt;
	}

	public void setDebt(int debt) {
		this.debt = debt;
	}

	public int getFine() {
		return fine;
	}

	public void setFine(int fine) {
		this.fine = fine;
	}
	
	@Override
	public String toString() {
		return "Catalog [key=" + key + ", keyBook=" + keyBook + ", keyReader=" + keyReader + ", dateTaken=" + dateTaken
				+ ", dateReturn=" + dateReturn + ", debt=" + debt + ", fine=" + fine + "]";
	}

}
