package ua.khpi.ropay.jdbs.items;

public class Books {
	
	private int idBook;
	private String bookName;
	private String writer;
	private String yearOfIssude;
	private String genre;
	
	public Books() {
		
	}
	
	public Books(int idBook, String bookName, String writer, String yearOfIssude, String genre) {
		
		this.idBook = idBook;
		this.bookName = bookName;
		this.writer = writer;
		this.yearOfIssude = yearOfIssude;
		this.genre = genre;		
	}
	
	
	public int getIdBook() {
		return idBook;
	}
	public void setIdBook(int idBook) {
		this.idBook = idBook;
	}
	public String getBookName() {
		return bookName;
	}
	public void setBookName(String bookName) {
		this.bookName = bookName;
	}
	public String getWriter() {
		return writer;
	}
	public void setWriter(String writer) {
		this.writer = writer;
	}
	public String getYearOfIssude() {
		return yearOfIssude;
	}
	public void setYearOfIssude(String yearOfIssude) {
		this.yearOfIssude = yearOfIssude;
	}
	public String getGenre() {
		return genre;
	}
	public void setGenre(String genre) {
		this.genre = genre;
	}

	@Override
	public String toString() {
		return "Books [idBook=" + idBook + ", bookName=" + bookName + ", writer=" + writer + ", yearOfIssude="
				+ yearOfIssude + ", genre=" + genre + "]";
	}

}
