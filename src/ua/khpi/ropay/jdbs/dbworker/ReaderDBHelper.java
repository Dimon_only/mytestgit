package ua.khpi.ropay.jdbs.dbworker;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import ua.khpi.ropay.jdbs.items.Readers;

public class ReaderDBHelper extends DBhelper<Readers> {

	public ReaderDBHelper(Connection connection) {
		super(connection);
	}

	@Override
	public String getInsertQuery() {
		return "INSERT INTO readers (key_reader, name_reader, surname, middle_name, "
				+ "phone_number, adress, year_of_birth) VALUES (?, ?, ?, ?, ?, ?, ?)";
	}

	@Override
	public String getUpdateQuery() {
		return "UPDATE readers SET key_reader = ?, name_reader = ?, surname = ?, "
				+ "middle_name = ?, phone_number = ?, adress = ?, year_of_birth = ?";
	}

	@Override
	public String getSelectQuery() {
		return "SELECT r.key_reader, r.name_reader as name_reader,"
				+ " r.surname as surname, r.middle_name as middle_name, r.phone_number as phone_number, \n"
				+ "r.adress as adress, r.year_of_birth as year_of_birth FROM readers r;";
	}

	@Override
	public String getDeleteQuery() {
		return "DELETE FROM readers WHERE key_reader = ?;";
	}

	@Override
	protected void prepareStatementForInsert(PreparedStatement statement, Readers object) {
		try {
			statement.setInt(1, object.getKeyReader());
			statement.setString(2, object.getNameReader());
			statement.setString(3, object.getSurname());
			statement.setString(4, object.getMiddleName());
			statement.setString(5, object.getPhoneNumber());
			statement.setString(6, object.getAdress());
			statement.setString(7, object.getYearOfBirth());
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	@Override
	protected void prepareStatementForUpdate(PreparedStatement statement, Readers object) {
		try {
			statement.setString(1, object.getNameReader());
			statement.setString(2, object.getSurname());
			statement.setString(3, object.getMiddleName());
			statement.setString(4, object.getPhoneNumber());
			statement.setString(5, object.getAdress());
			statement.setString(6, object.getYearOfBirth());
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	@Override
	protected void prepareStatementForDelete(PreparedStatement statement, Readers object) {
		try {
			
			statement.setInt(1, object.getKeyReader());
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	@Override
	public List<Readers> parseResultSet(ResultSet rs) {
		LinkedList<Readers> result = new LinkedList<>();
		try {
            while (rs.next()) {
            	Readers readers = new Readers();
            	readers.setKeyReader(rs.getInt("key_reader"));
            	readers.setNameReader(rs.getString("name_reader"));
            	readers.setSurname(rs.getString("surname"));
            	readers.setMiddleName(rs.getString("middle_name"));
            	readers.setPhoneNumber(rs.getString("phone_number"));
            	readers.setAdress(rs.getString("adress"));
            	readers.setYearOfBirth(rs.getString("year_of_birth"));
            	result.add(readers);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
		
		return result;
	}

}
