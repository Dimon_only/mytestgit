package ua.khpi.ropay.jdbs.dbworker;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import ua.khpi.ropay.jdbs.items.Books;

public class BookDBHelper extends DBhelper<Books> {

	public BookDBHelper(Connection connection) {
		super(connection);
	}

	@Override
	public String getInsertQuery() {
		return "INSERT INTO books (key_book, book_name, writer, year_of_issue, genre) VALUES (?, ?, ?, ?, ?);";
	}

	@Override
	public String getUpdateQuery() {
		return "UPDATE books \n" + "SET book_name = ?, writer = ?, year_of_issue = ?, genre = ? \n"
				+ "WHERE key_book = ?;";
	}

	@Override
	public String getSelectQuery() {
		return "SELECT b.key_book, b.book_name as book_name,"
				+ " b.writer as writer, b.year_of_issue as year_of_issue, b.genre as genre\n"
				+ "FROM books b;";
	}

	@Override
	public String getDeleteQuery() {
		return "DELETE FROM books WHERE key_book = ?;";
	}

	@Override
	public void prepareStatementForInsert(PreparedStatement statement, Books object) {
		try {
			statement.setInt(1, object.getIdBook());
			statement.setString(2, object.getBookName());
			statement.setString(3, object.getWriter());
			statement.setString(4, object.getYearOfIssude());
			statement.setString(5, object.getGenre());

		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	@Override
	protected void prepareStatementForUpdate(PreparedStatement statement, Books object) {

		try {
			statement.setString(1, object.getBookName());
			statement.setString(2, object.getWriter());
			statement.setString(3, object.getYearOfIssude());
			statement.setString(4, object.getGenre());
			
		}catch (SQLException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void prepareStatementForDelete(PreparedStatement statement, Books object) {
		 try {
	            statement.setInt(1, object.getIdBook());
	        } catch (SQLException e) {
	            e.printStackTrace();
	        }
	}

	@Override
	public List<Books> parseResultSet(ResultSet rs) {
		LinkedList<Books> result = new LinkedList<>();
        try {
            while (rs.next()) {
            	Books books = new Books();
            	books.setIdBook(rs.getInt("key_book"));
            	books.setBookName(rs.getString("book_name"));
            	books.setWriter(rs.getString("writer"));
            	books.setYearOfIssude(rs.getString("year_of_issue"));
            	books.setGenre(rs.getString("genre"));
                result.add(books);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
	}

}
