package ua.khpi.ropay.jdbs.dbworker;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;

import ua.khpi.ropay.jdbs.items.Catalog;

public class CatalogDBHelper extends DBhelper<Catalog> {

	public CatalogDBHelper(Connection connection) {
		super(connection);
	}

	@Override
	public String getInsertQuery() {
		return "INSERT INTO catalog (queryKey, key_book, key_reader, dateIn, "
				+ "dateOut, debt, fine) VALUES (?, ?, ?, ?, ?, ?, ?)";
	}

	@Override
	public String getUpdateQuery() {
		return "UPDATE readers SET key_reader = ?, name_reader = ?, surname = ?, "
				+ "middle_name = ?, phone_number = ?, adress = ?, year_of_birth = ?";
	}

	@Override
	public String getSelectQuery() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getDeleteQuery() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected void prepareStatementForInsert(PreparedStatement statement, Catalog object) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void prepareStatementForUpdate(PreparedStatement statement, Catalog object) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void prepareStatementForDelete(PreparedStatement statement, Catalog object) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<Catalog> parseResultSet(ResultSet rs) {
		// TODO Auto-generated method stub
		return null;
	}

}
