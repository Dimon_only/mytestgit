package ua.khpi.ropay.jdbs.dbworker;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

public abstract class DBhelper<T> {

	Connection connection;

	public DBhelper(Connection connection) {
		this.connection = connection;
	}

	/**
	 * Returns a sql query to insert a new record into the database.
	 *
	 * @return string representation of query INSERT INTO [Table] ([column, column,
	 *         ...]) VALUES (?, ?, ...);
	 */
	public abstract String getInsertQuery();

	/**
	 * Returns a sql query to update a record in the database.
	 *
	 * @return string representation of query UPDATE [Table] SET [column = ?, column
	 *         = ?, ...] WHERE id = ?;
	 */
	public abstract String getUpdateQuery();

	/**
	 * Returns a sql query to retrieve all records.
	 *
	 * @return string representation of query SELECT * FROM [Table]
	 */
	public abstract String getSelectQuery();

	/**
	 * Returns a sql query to delete record from database
	 *
	 * @return string representation of query DELETE FROM [Table] WHERE id= ?;
	 */
	public abstract String getDeleteQuery();

	/**
	 * Sets the insert arguments of the query according to the value of the fields
	 * of the object.
	 *
	 * @param statement
	 *            statement to which we apply the parameters
	 * @param object
	 *            the object from which we take the parameters
	 */
	protected abstract void prepareStatementForInsert(PreparedStatement statement, T object);

	/**
	 * Sets the update arguments of the query according to the value of the fields
	 * of the object.
	 *
	 * @param statement
	 *            statement to which we apply the parameters
	 * @param object
	 *            the object from which we take the parameters
	 */
	protected abstract void prepareStatementForUpdate(PreparedStatement statement, T object);

	/**
	 * Sets the delete arguments of the query according to the value of the fields
	 * of the object.
	 *
	 * @param statement
	 *            statement to which we apply the parameters
	 * @param object
	 *            the object from which we take the parameters
	 */
	protected abstract void prepareStatementForDelete(PreparedStatement statement, T object);

	/**
	 * Parses the ResultSet and returns a list of objects corresponding to the
	 * contents of the ResultSet.
	 *
	 * @param rs
	 *            result set which we parse
	 * @return list of objects of type T
	 */
	public abstract List<T> parseResultSet(ResultSet rs);

	/**
	 * Selects all records from database.
	 *
	 * @return list of objects of type T
	 */
	public List<T> selectAll() {
		List<T> list = new LinkedList<T>();
		String sql = getSelectQuery();
		try (PreparedStatement statement = connection.prepareStatement(sql)) {
			ResultSet rs = statement.executeQuery();
			list = parseResultSet(rs);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;

	}

	/**
	 * Selects one records from database with certain primary key.
	 *
	 * @param key
	 *            - key of object which required
	 * @return list of objects of type T
	 * @throws Exception
	 *             in case if record not found or received more than one record
	 */
	public T selectByPK(Integer key) throws Exception {
		List<T> list = null;
		String sql = getSelectQuery().replaceAll(";", "");
		sql += " WHERE key_book = ?";
		System.out.println(sql);
		try (PreparedStatement statement = connection.prepareStatement(sql)) {
			statement.setInt(1, key);
			ResultSet rs = statement.executeQuery();
			list = parseResultSet(rs);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		if (list == null || list.size() == 0) {
			throw new Exception("Record with PK = " + key + " not found.");
		}
		if (list.size() > 1) {
			throw new Exception("Received more than one record.");
		}
		return list.iterator().next();
	}

	/**
	 * Inserts one records if insert successfully returns record which was inserted.
	 *
	 * @param object
	 *            - object of object which inserted
	 * @return object which we insert
	 */
	public T insert(T object) {
		// T persistInstance = null;
		// Добавляем запись
		String sql = getInsertQuery();
		try (PreparedStatement statement = connection.prepareStatement(sql)) {
			prepareStatementForInsert(statement, object);
			int count = statement.executeUpdate();
			if (count != 1) {
				throw new SQLException("On insert more then 1 record: " + count);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		// Получаем только что вставленную запись TODO FIX
		/*
		 * sql = getSelectQuery() + " WHERE id = key_book;"; try (PreparedStatement
		 * statement = connection.prepareStatement(sql)) { ResultSet rs =
		 * statement.executeQuery(); List<T> list = parseResultSet(rs); if ((list ==
		 * null) || (list.size() != 1)) { throw new
		 * Exception("Exception on findByPK new persist data."); } persistInstance =
		 * list.iterator().next(); } catch (Exception e) { e.printStackTrace(); }
		 */
		return null;
	}

	/**
	 * Deletes one record by primary key.
	 *
	 * @param object
	 *            - object of object which deleted
	 */
	public void delete(T object) {
		String sql = getDeleteQuery();
		try (PreparedStatement statement = connection.prepareStatement(sql)) {
			prepareStatementForDelete(statement, object);
			int count = statement.executeUpdate();
			if (count != 1) {
				throw new SQLException("On delete modify more then 1 record: " + count);
			}
			statement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Updates one record by primary key.
	 *
	 * @param object
	 *            - object of object which updated
	 */
	public void update(T object) {
		String sql = getUpdateQuery();
		try (PreparedStatement statement = connection.prepareStatement(sql);) {
			prepareStatementForUpdate(statement, object); // заполнение аргументов запроса оставим на совесть потомков
			int count = statement.executeUpdate();
			if (count != 1) {
				throw new SQLException("On update modify more then 1 record: " + count);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
