package ua.khpi.ropay.jdbs.connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class MySqlFactory {
	
	private static final String URL = "jdbc:mysql://localhost:3306/JDBS?useSSL=false";
    private static final String USER = "root";
    private static final String PASSWORD = "31415";
    private static final String DRIVER = "com.mysql.jdbc.Driver";

    public MySqlFactory() {
        try {
            Class.forName(DRIVER);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public Connection getConnection() {
        Connection connection = null;
        try {
            connection = DriverManager.getConnection(URL, USER, PASSWORD);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return connection;
    }

}
