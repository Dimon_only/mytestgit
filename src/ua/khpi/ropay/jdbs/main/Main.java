package ua.khpi.ropay.jdbs.main;

import java.sql.Connection;
import ua.khpi.ropay.jdbs.connection.MySqlFactory;
import ua.khpi.ropay.jdbs.dbworker.BookDBHelper;
import ua.khpi.ropay.jdbs.dbworker.ReaderDBHelper;
import ua.khpi.ropay.jdbs.items.Books;
import ua.khpi.ropay.jdbs.items.Readers;

public class Main {

	public static void main(String[] args) {
		MySqlFactory msf = new MySqlFactory();
		
		Connection connection = msf.getConnection();
		
		BookDBHelper bookDBHelper = new BookDBHelper(connection);
		ReaderDBHelper readerDBHelper = new ReaderDBHelper(connection);
		
		Books books = new Books(2333, "ff", "dd", "0000", "Saa");
		
		Readers readers = new Readers(111, "Анна", "Sobol", "Nicolaevna", "+380669736543", "Kupyansk Moskovskaya street", "1999-01-15");
		
		
			
			//System.out.println(bookDBHelper.selectAll());
			try {
				//System.out.println(bookDBHelper.selectByPK(3347));
				//bookDBHelper.delete(books);
				//bookDBHelper.insert(books);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			//bookDBHelper.insert(books);
			
			//readerDBHelper.insert(readers);
			readerDBHelper.delete(readers);
			System.out.println(readerDBHelper.selectAll());
			
			
			
		
	}
}
